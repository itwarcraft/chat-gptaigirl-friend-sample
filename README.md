# chatGPTAIGirlFriendSample

#### 介绍
使用chatGPT+unity制作的简单的AI女友(老婆)对话机器人的demo。
使用前须知：请从仓库中选择需要的工程包下载使用，如果选用的是微软Azure语音合成的工程包，需要下载微软语音合成的SDK，导入工程
工程包含：

1、chatgpt-ai-girl-friend-sample.unitypackage：此包使用的是gpt3的模型，以及微软语音合成

2、[gpt-3.5-Turbo]chatgpt-ai-girl-friend-sample.unitypackage：此包使用的是gpt-3.5-turbo的模型，以及微软语音合成

3、[百度AI开放平台]chatgpt-ai-girl-friend-sample.unitypackage：此包使用的是gpt-3.5-turbo的模型，以及百度语音合成api


#### 使用说明

 **如果使用了上述1、2两个包，需依赖以下两个插件，请下载并导入以下插件后，再导入工程包，不然会有报错的。
** 
1.  下载VRoid模型导入unity的插件包，并导入到你的工程文件里
插件包地址：https://github.com/vrm-c/UniVRM
下载最新的release版本的插件，导入到你的工程文件里
![输入图片说明](Vrm%20Plugin.png)

2.  下载微软Azure语音合成，unity插件包
打开微软Azure语音合成，关于SDK安装的说明：https://learn.microsoft.com/zh-cn/azure/cognitive-services/speech-service/quickstarts/setup-platform?pivots=programming-language-csharp&tabs=windows%2Cubuntu%2Cunity%2Cjre%2Cmaven%2Cnodejs%2Cmac%2Cpypi
从中找到unity的SDK插件包，下载并导入到你的工程文件里
![输入图片说明](image.png)

3.  下载本仓库提供的*.unitypackage文件，并导入到你的工程文件里

-------------------------------------------------------------
2023.4.15 更新

1、更新了[gpt-3.5-Turbo]chatgpt-ai-girl-friend-sample.unitypackage，示例代码里增加了azure语音识别的功能。
在示例场景里，运行时，按住空格键启动麦克风输入，松开空格键完成录制并进行语音识别，识别成功不需要再点击发送按钮。

2、新增了[4.15]AzureRecognize.unitypackage包，只包含azure语音识别的代码示例，可以参考学习


